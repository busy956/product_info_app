import 'package:flutter/material.dart';
import 'package:product_info_app/model/product_item.dart';

class ComponentProductItem extends StatelessWidget {
  const ComponentProductItem({
    super.key,
    required this.productItem,
    required this.callback
  });

  final ProductItem productItem;
  final VoidCallback callback;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Text(productItem.imageTitle),
            Text(productItem.name),
            Text('${productItem.price}'),
          ],
        ),
      ),
    );
  }
}
