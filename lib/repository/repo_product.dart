import 'package:dio/dio.dart';
import 'package:product_info_app/config/config_api.dart';
import 'package:product_info_app/model/product_list_result.dart';

class RepoProduct {
  /**
   * 복수R
   */

  Future<ProductListResult> getProducts() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/product/all'; // 엔드포인트

    final response = await dio.get(
        _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return ProductListResult.fromJson(response.data);
  }

  Future<ProductListResult> getProduct(num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/product/detail/{id}';

    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()) // 찾아서 바꾸기
      options: Options(
        followRedirects: false,
      validateStatus: (status) {
          return status == 200;
      }));

    return ProductListResult.fromJson(response)
  }
}