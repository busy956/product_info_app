class ProductItem{
  num id;
  String imageTitle;
  String name;
  num price;

  ProductItem(this.id, this.imageTitle, this.name, this.price);

  factory ProductItem.fromJson(Map<String, dynamic>json) {
    return ProductItem(
      json['id'],
      json['imageTitle'],
      json['name'],
      json['price'],
    );
  }
}