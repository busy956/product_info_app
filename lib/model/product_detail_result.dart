import 'package:product_info_app/model/product_response.dart';

class ProductDetailResult {
  String msg;
  num code;
  ProductResponse data;

  ProductDetailResult(this.msg, this.code, this.data);

  factory ProductDetailResult.fromJson(Map<String, dynamic> json) {
    return ProductDetailResult(
      json['msg'],
      json['code'],
      ProductResponse.fromJson(json['data'])
    );
  }
}