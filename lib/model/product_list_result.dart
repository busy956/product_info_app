import 'package:product_info_app/model/product_item.dart';

class ProductListResult {
  String msg;
  num code;
  List<ProductItem> list;
  num totalCount;

  ProductListResult(this.msg, this.code, this.list, this.totalCount);

  factory ProductListResult.fromJson(Map<String, dynamic> json) {
    return ProductListResult(
      json['msg'],
      json['code'],
      json['list'] != null ? (json['list'] as List).map((e) => ProductItem.fromJson(e)).toList() : [],
      json['totalCount']
    );
    // json으로 [{name:'ghd'}, {name:'rla'}]이런 string을 받아서 Object의 List로 바꾸고 난 후에
    // 그럼 어쨋든 List니까 한 뭉탱이씩 던져줄 수 있다.
    // 근데.. 한뭉탱이씩 던지겠다 하면서 한 뭉탱이를 부르는 이름이 바로 e이다
    // 다 작은그릇으로 바꾼 다음에
    // 다시 그것들을 싹 가져와서 리스트로 촥 하고 정리해줌
  }
}