import 'package:product_info_app/model/product_item.dart';

class ProductResponse {
  num id;
  String imageTitle;
  String name;
  num price;

  ProductResponse(this.id, this.imageTitle, this.name, this.price);

  factory ProductResponse.fromJson(Map<String, dynamic> json) {
    return ProductResponse(
      json['id'],
      json['imageTitle'],
      json
    );
  }
}