import 'package:flutter/material.dart';
import 'package:product_info_app/components/component_product_item.dart';
import 'package:product_info_app/model/product_item.dart';
import 'package:product_info_app/repository/repo_product.dart';

class PageProductList extends StatefulWidget {
  const PageProductList({super.key});

  @override
  State<PageProductList> createState() => _PageProductListState();
}

class _PageProductListState extends State<PageProductList> {
  List<ProductItem> _list = [];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에..
    // setstate해서 _list 교체 할거다....
    await RepoProduct().getProducts()
        .then((res) => {
          print(res),
          setState(() {
          _list = res.list;
          })
        })
        .catchError((err) => {
          debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('책 리스트'),
      ),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int idx) { //공사장에 대한 정보, 몇번째 벽돌인지
          return ComponentProductItem(productItem: _list[idx], callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageProductList()));
          });
        },
      ),

    );
  }
}
