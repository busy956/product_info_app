import 'package:flutter/material.dart';
import 'package:product_info_app/model/product_response.dart';
import 'package:product_info_app/repository/repo_product.dart';

class PageProductDetail extends StatefulWidget {
  const PageProductDetail({
    super.key,
    required this.id
  });

  final num id;

  @override
  State<PageProductDetail> createState() => _PageProductDetailState();
}

class _PageProductDetailState extends State<PageProductDetail> {
  ProductResponse? _detail; // ? 있을 수도 있고 없을 수도 있다(없으면 null)

  Future<void> _loadDetail() async {
    await RepoProduct().getProduct(widget.id)
        .then((res) => {
          setState(() {
            _detail = res.data;
          })
    });
  }

  void initState() {
    super.initState();
    _loadDetail();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상세보기'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if(_detail == null) {
      return Text('데이터 로딩중'); //스켈레톤 ui참고
    } else {
    return ListView(
      children: [
        Text('${widget.id}'),
        Text(_detail!.imageTitle),
        Text(_detail!.name),
        Text('${_detail!.price}'),
      ],
    );
  }
}
